export default {
  pages: [],
  window: {
    backgroundTextStyle: "light",
    navigationBarTextStyle: "black",
    navigationBarBackgroundColoe: "#fff",
  },
  sitemapLocation: "sitemap.json",
};
