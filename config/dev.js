const { merge } = require("webpack-merge");
const common = require("./index.js");

module.exports = merge(common, {
  devtool: "inline-source-map",
  devServer: {
    contentBase: "../../dist",
    hot: true,
  },
  mode: "development",
});
