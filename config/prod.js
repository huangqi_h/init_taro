const   TerserPlugin = require("terser-webpack-plugin");
const { webpack } = require("webpack");
const { merge } = require("webpack-merge");
const common = require("./index.js");


module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
      test: /\.js(\?.*)?$/i,
      parallel: true,
      terserOptions: {
        output: {comments: false}
      },
      extractComments: true
    })]
  },
  // plugins: [
  //   new webpack.DefinePlugin({
  //     'process.env.NODE_ENV': JSON.stringify('production')
  //   })
  // ]
})
